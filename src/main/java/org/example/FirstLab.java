package org.example;

import io.github.bonigarcia.wdm.WebDriverManager;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.time.Duration;

public class FirstLab {
    private WebDriver chromeDriver;
    private static final String baseUrl = "https://www.nmu.org.ua/ua";

    @BeforeClass(alwaysRun = true)
    public void setUp(){
        WebDriverManager.chromedriver().setup();
        ChromeOptions options = new ChromeOptions();
        options.setImplicitWaitTimeout(Duration.ofSeconds(13));
        this.chromeDriver = new ChromeDriver();

    }

    @BeforeMethod
    public void preconditions(){
        chromeDriver.get(baseUrl);
    }

    @AfterClass(alwaysRun = true)
    public void tearDown(){
        chromeDriver.quit();
    }

    @Test
    public void testHeaderExists(){
        WebElement header = chromeDriver.findElement(By.id("header"));
        Assert.assertNotNull(header);
    }

    @Test
    public void testClickOnForStudent(){
        WebElement button = chromeDriver.findElement(By.xpath("/html/body/center/div[4]/div/div[1]/ul/li[4]/a"));

        Assert.assertNotNull(button);

        button.click();

        Assert.assertNotEquals(chromeDriver.getCurrentUrl(), baseUrl);
    }
@Test
    public void testSearchFieldOnForStudentPage(){
        String url = "content/student_life/students/";
        chromeDriver.get(baseUrl + url);
        WebElement element= chromeDriver.findElement(By.tagName("input"));
        Assert.assertNotNull(element);
        System.out.println(String.format("Name attribute: %s", element.getAttribute("name")) +
                String.format("ID attribute: %s", element.getAttribute("id")) +
                String.format("Type attribute: %s", element.getAttribute("type")) +
                String.format("Value attribute: %s", element.getAttribute("value")) +
                String.format("Position: (%d;%d)", element.getLocation().x, element.getLocation().y) +
                String.format("Size: %dx%d", element.getSize().height, element.getSize().width)
                );

        String inputValue = "I need info";
        element.sendKeys(inputValue);

        Assert.assertEquals(element.getText(), inputValue);

        element.sendKeys(Keys.ENTER);
        Assert.assertNotEquals(chromeDriver.getCurrentUrl(), url);
    }

    @Test
    public void testSlider(){
        WebElement nextButton = chromeDriver.findElement(By.className("next"));
        WebElement nextButtonByCss = chromeDriver.findElement(By.cssSelector("a.next"));

        Assert.assertEquals(nextButton, nextButtonByCss);

        WebElement previousButton = chromeDriver.findElement(By.className("prev"));

        for (int i = 0;i < 20; i++){
            if (nextButton.getAttribute("class").contains("disabled")){
                previousButton.click();
                Assert.assertTrue(previousButton.getAttribute("class").contains("disabled"));
                Assert.assertFalse(nextButton.getAttribute("class").contains("disabled"));
            }
            else{
                nextButton.click();
                Assert.assertTrue(nextButton.getAttribute("class").contains("disabled"));
                Assert.assertFalse(previousButton.getAttribute("class").contains("disabled"));
            }
        }
    }
}
