package org.example;

import io.github.bonigarcia.wdm.WebDriverManager;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.time.Duration;

public class TestRozetka {
    private WebDriver driver;
    private static final String baseUrl="https://rozetka.com.ua/";
    @BeforeClass
    public void setUp() {
        WebDriverManager.chromedriver().setup();
        ChromeOptions options = new ChromeOptions();
        options.setImplicitWaitTimeout(Duration.ofSeconds(13));

        driver = new ChromeDriver(options);
    }
    @Test
    public void testClickSomething(){
        WebElement element = driver.findElement(By.xpath("/html/body/app-root/div/div/rz-header/rz-main-header/header/div/div/ul/li[7]/rz-cart/button"));

        element.click();

        Assert.assertNotEquals(driver.getCurrentUrl(), baseUrl);
    }
    @Test
    public void testInputData(){
        WebElement inputField = driver.findElement(By.name("search"));
        String inputData = "Тестові дані";
        inputField.sendKeys(inputData);
        String fieldText = inputField.getAttribute("value");
        Assert.assertEquals(fieldText, inputData);
    }
    @Test
    public void testSomeContition(){
        WebElement element = driver.findElement(By.xpath("/html/body/app-root/div/div/rz-header/rz-main-header/header/div/div/a/picture/img"));
        boolean condition = element.isDisplayed();
        Assert.assertTrue(condition);
    }
    @BeforeMethod
    public void preconditions(){
        driver.get(baseUrl);
    }
    @AfterClass
    public void tearDown() {
        // Закриваємо веб-драйвер після виконання тесту
        driver.quit();
    }
}